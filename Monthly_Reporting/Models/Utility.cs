﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Monthly_Reporting.Properties;
using System.Configuration;
using System.Data.OleDb;
using Monthly_Reporting.Core;
using ClosedXML.Excel;
using System.IO;
using Monthly_Reporting.Models;
using System.Web.Mvc;
using System.Net;

namespace Monthly_Reporting.Models
{
    public class Utility
    {
        SqlConnection con = new SqlConnection();
        public static SqlConnection sqlConnection;
        public DataTable CoreReport(Reports objInfo)
        {
            DataTable dtCoreReport = new DataTable();
            ActResult xresult = new ActResult();
            string StrType = "";
            string SqlObj = "";
            SqlCommand CommandName;
            StrType = this.dbSingleResult("select strType from Table_Reports where Report_Code='"+ objInfo.ReportCode+"'");
            SqlObj = this.dbSingleResult("select StrSQL from Table_Reports where Report_Code='" + objInfo.ReportCode + "'");
            CommandName = new SqlCommand("select CommandName from Table_Reports where Report_Code='" + objInfo.ReportCode + "'");
            switch (StrType.ToLower())
            {
                
                case "store":
                    SqlConnection conn = ServerConnection();
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    command = conn.CreateCommand();
                    command.Parameters.Clear();
                    command.CommandType = CommandType.StoredProcedure;                                     
                    command.CommandText = SqlObj;
                    CommandDisb(command, objInfo);
                    SqlDataAdapter adpter = new SqlDataAdapter(command);
                    try
                    {
                        adpter.Fill(dtCoreReport);
                        xresult.Result = ActResult.ResultType.Success;
                        return dtCoreReport;
                    }
                    catch (Exception ex)
                    {
                        string test = ex.ToString();
                        xresult.Result = ActResult.ResultType.Fail;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    break;

                case "strsql":
                    xresult = this.ExecuteTable(SqlObj,con,null,out dtCoreReport);
                    if(xresult.Result == ActResult.ResultType.Success)
                    {
                        return dtCoreReport;
                    }
                break;
                case "view":

                break;
            }
            return dtCoreReport;
        }
        private void CommandDisb(SqlCommand command,Reports rs)
        {
            command.Parameters.Add("@Mode", SqlDbType.Int).Value = rs.mode;
            command.Parameters.Add("@StartDisbDate", SqlDbType.DateTime).Value = rs.datestart;
            command.Parameters.Add("@EndDisbDate", SqlDbType.DateTime).Value = rs.dateend;
            command.Parameters.Add("@ReportDate", SqlDbType.DateTime).Value = rs.reportdate;
            command.Parameters.Add("@BrCode", SqlDbType.VarChar).Value = rs.BrCode;
            command.Parameters.Add("@DisbDate", SqlDbType.DateTime).Value = rs.disbdate;
            command.Parameters.Add("@BranchType", SqlDbType.VarChar).Value = rs.CompanyType;
            command.Parameters.Add("@COID", SqlDbType.VarChar).Value = rs.CoId;
        }
        
        public string ArrayOfZoneList(string UserKey)
        {
            string branchZone = Convert.ToString(this.dbSingleResult("select branch_zone from users where user_key='"+UserKey+"'"));
            var zone_array = branchZone.Split(',');
            string zonetoarray = string.Join("','", zone_array.Skip(0));

            return zonetoarray;

        }
        /*
            1-Get Location Code
            2-
        */
        public DataTable LocationCode(int Mode,int setType,string Code)
        {
            DataTable dtLocationCode = new DataTable();

            string sql = @"exec LocationCode '"+Mode+"','"+setType+"','"+Code+"'";
            dtLocationCode = this.dbResult(sql);
            return dtLocationCode;
        }
        public string ArrayOfReportList(string ReportCode)
        {

            string theString = Convert.ToString(ReportCode);
            var array = theString.Split(',');
            string firstElem = array.First();
            string ListOfReportCode = string.Join("','", array.Skip(0));
            return ListOfReportCode;
        }
        //Manger by zone
        public DataTable ZoneControl(string Zonelist)
        {
            DataTable dtZone = new DataTable();
            string sql = @"select *,0 as BalAmt from BRANCH_LISTS where flag=1 and BrCode in('" + Zonelist + "')";
            dtZone = this.dbResult(sql);
            return dtZone;

        }

        public DataTable ConsolidateAdminFee(Reports rs,string userkey)
        {


            DataTable dtstatus = new DataTable();
            string BrZone_loop = Convert.ToString(dbSingleResult("select branch_zone from users where user_key='"+ userkey + "'"));
            var zone_array_loop = BrZone_loop.Split(',');
            string zonetoarray_loop = string.Join("','", zone_array_loop.Skip(0));

            string SQl_loop = "";
            if(rs.BrCode == "ALL")
            {
                SQl_loop = "select row_number() over(order by BL.BrCode) as id,*  ,1 as checked ,'0' as statusconnect from  BRANCH_LISTS BL inner join VPN V ON V.BrCode=BL.BrCode where BL.flag=1 and BL.BrCode in('" + zonetoarray_loop + "')";
            }
            else
            {
                SQl_loop = "select row_number() over(order by BL.BrCode) as id,*  ,1 as checked ,'0' as statusconnect from  BRANCH_LISTS BL inner join VPN V ON V.BrCode=BL.BrCode where BL.flag=1 and BL.BrCode='"+rs.BrCode+"'";
            }
            
            dtstatus = dbResult(SQl_loop);

            SqlConnection sqlcon = new SqlConnection();          
            DataSet ds = new DataSet();
            DataTable dtAdminFee = new DataTable();
            ActResult xresult = new ActResult();
            DataTable dtPublic = new DataTable();
            DataRow row;
            string sql = @"
                            /*Declare Varible*/          
                            declare @BrCode nvarchar(10)
                            declare @BrShort nvarchar(10)
                            declare @BrName nvarchar(50)
                            declare @DbName nvarchar(50)

                            /*Set Value or Set Varible*/
                            set @DbName = (SELECT DB_NAME())

                            select 
                              @BrCode = SubBranchCode,
                              @BrShort = SubBranchID,
                              @BrName = SubBranchNameLatin
                            from skp_brlist 
                            where DBName = (select left(@DbName,len(@DbName)-4))
                            
                            /* end */

  
                            select
                              row_number() over(order by Acc) as ID,
                              @BrCode as BrCode,
                              @BrShort as BrName,
                              Acc
                                    ,CoName
                                    ,ClientName                          
                                    ,AdminFee_Paid
                                    ,AdminFee_Remin  as AdminFeeRemind
                                    ,Different                                    
                              ,TrnDate
                              ,Reason                        
                              ,case when flag=1 then 'Pending'
                                when flag=0 then 'Approved'
                                when flag=-1 then 'Rejected'
                                when flag=-2 then 'Forget Add AdminFee'
                              end as Status
                              ,Note              
                              from skp_adminfee              
                                    where 
                                    TrnDate = '" + string.Format("{0:yyyy-MM-dd}",rs.reportdate) + "';";


            foreach (DataRow dr in dtstatus.Rows)
            {
                sqlcon = BrConnection(dr["FullDbName"].ToString(), "sa", "mbwin", dr["VPN"].ToString());
                if (sqlcon.State != ConnectionState.Open)
                {
                    try
                    {
                        sqlcon.Open();
                        xresult = ExecuteTable(sql, sqlcon, null, out dtAdminFee);
                        if(xresult.Result == ActResult.ResultType.Success)
                        {
                            if (dtAdminFee.Rows.Count > 0)
                            {
                                ds.Tables.Add(dtAdminFee);
                                
                            }
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }

            }
            for(int i = 0; i < ds.Tables.Count; i++)
            {
                if (i == 0)
                    dtPublic = ds.Tables[i].Copy();
                else
                    dtPublic.Merge(ds.Tables[i]);
            }
            return dtPublic;
        }

        //Report List by Staff
        public DataTable ListReport(string ListReport)
        {
            DataTable dtList = new DataTable();
            string sql = @"select * from Table_Reports where flag=1 and Report_Code in('" + ListReport + "')";
            dtList = this.dbResult(sql);
            return dtList;
        }
          
        //Branch Conneciton
        public SqlConnection LocalConnection()
        {
            SqlConnection con = new SqlConnection();
            DbName = Settings.Default["DbName"].ToString();
            UserName = Settings.Default["UserName"].ToString();
            Password = Settings.Default["Password"].ToString();
            ServerName = Settings.Default["ServerName"].ToString();
            con.ConnectionString = "Data source=" + ServerName + ";Initial catalog=" + DbName + ";User ID=" + UserName + ";Password=" + Password;
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            return con;

        }
        public ActResult ExecuteNoneQueryOLEDB(string SQL)
        {
            
            ActResult _result = new ActResult();
            SqlConnection sqlcon = LocalConnection();
            SqlCommand command = new SqlCommand();
            command.CommandText = SQL;
            command.CommandType = CommandType.Text;
            command.Connection = sqlcon;
            try
            {
                if (sqlcon.State != ConnectionState.Open)
                {
                    sqlcon.Open();
                }
                command.ExecuteNonQuery();
                _result.Result = ActResult.ResultType.Success;
            }
            catch (Exception ex)
            {
                //Console.WriteLine("\r\n Error to Execute Command \r\n" + ex.Message);
                _result.Message = "\r\n Error to Execute Command \r\n" + ex.Message;
                _result.Result = ActResult.ResultType.Fail;
            }
            finally
            {
                sqlcon.Close();
            }
            return _result;
        }
        //Server Connection or HO Connection
        public SqlConnection ServerConnection()
        {
            SqlConnection con = new SqlConnection();
            DbName = Settings.Default["DbName"].ToString();
            UserName = Settings.Default["UserName"].ToString();
            Password = Settings.Default["Password"].ToString();
            ServerName = Settings.Default["ServerName"].ToString();
            con.ConnectionString = "Data source=" + ServerName + ";Initial catalog=" + DbName + ";User ID=" + UserName + ";Password=" + Password;           
            return con;
        }
       //Branch Connection from Branch to HO
       public SqlConnection BrConnection(string DbName,string UserName,string Password,string ServerName)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = "Data source=" + ServerName + ";Initial catalog=" + DbName + ";User ID=" + UserName + ";Password=" + Password;
          
            return con;
        }

        public string ServerName { get; set; }
        public string DbName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        
        public ActResult ExecuteTable(string SQL, SqlConnection sqlConn, SqlTransaction Trans, out DataTable dt)
        {
            ActResult actResult = new ActResult();
            dt = new DataTable();
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.Connection = sqlConn;
            command.Transaction = Trans;
            command.CommandText = SQL;
            SqlDataAdapter adpter = new SqlDataAdapter(command);
            try
            {
                adpter.Fill(dt);
                actResult.Result = ActResult.ResultType.Success;
            }
            catch (Exception)
            {
                actResult.Result = ActResult.ResultType.Fail;
            }

            return actResult;
        }
        public DataTable dbBranchResult(string sqlstring,SqlConnection sqlConn)
        {
            DataTable dtResult = new DataTable();
            ActResult xresult = new ActResult();
           
            string sql = sqlstring;
            xresult = ExecuteTable(sql, sqlConn, null, out dtResult);
            if (xresult.Result == ActResult.ResultType.Success)
            {
                if (dtResult.Rows.Count > 0)
                {
                    return dtResult;
                }
                else
                {
                    return dtResult;
                }
            }
            return dtResult;
        }
        public DataTable dtOnlyOneBranch(string strsql, string brcode)
        {

            DataTable dtresult = new DataTable();
            DataTable dtBrcode = new DataTable();
            SqlConnection sqlcon = new SqlConnection();

            string brlist = @"select * from BRANCH_LISTS BL
                           inner join VPN V ON V.BrCode=BL.BrCode where BL.flag=1 and BL.BrCode in('" + brcode + "')";
            
            dtBrcode = dbResult(brlist);
            foreach (DataRow dr in dtBrcode.Rows)
            {
                sqlcon = BrConnection(dr["FullDbName"].ToString(), "sa", "Sa@#$Mbwin", dr["VPN"].ToString());
                
                if (sqlcon.State != ConnectionState.Open)
                {
                    dtresult = dbBranchResult(strsql, sqlcon);
                    return dtresult;
                }
            }

            return dtresult;
        }

        public DataTable dbResult(string sqlstring)
        {
            DataTable dtResult = new DataTable();
            ActResult xresult = new ActResult();
            SqlConnection conn = ServerConnection();
            conn.Open();
            string sql = sqlstring;
            xresult = ExecuteTable(sql, conn, null, out dtResult);
            if (xresult.Result == ActResult.ResultType.Success)
            {
                if (dtResult.Rows.Count > 0)
                {
                    return dtResult;
                }
                else
                {
                    return dtResult;
                }
            }
            return dtResult;
        }
        public List<string> FTPConnection(string ServerName, string UserName, string Password, string BrShort, string rootfolder)
        {
            var name = new List<string>();
            try
            {
                string userName = UserName;
                string password = Password;
                string server = ServerName + "/" + BrShort + "/" + rootfolder;

                var request = (FtpWebRequest)WebRequest.Create(server);
                request.Credentials = new NetworkCredential(userName, password);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                return names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                //return request.Method;
            }
            catch (Exception)
            {

                return name;
            }


        }
        public string dbSingleResult(string sqlstring)
        {
            DataTable dtResult = new DataTable();
            ActResult xresult = new ActResult();
            SqlConnection conn = ServerConnection();
            string StrSql = "";
            conn.Open();
            string sql = sqlstring;
            xresult = ExecuteTable(sql, conn, null, out dtResult);
            if (xresult.Result == ActResult.ResultType.Success)
            {
                if (dtResult.Rows.Count > 0)
                {
                     foreach(DataRow dr in dtResult.Rows)
                    {
                        StrSql=dr[0].ToString();
                    }  
                }
                
            }
            return StrSql;
        }
        //public ActResult ExecuteTable(string SQL, SqlConnection sqlcon, out DataTable dt)
        //{
        //    dt = new DataTable();
        //    ActResult _result = new ActResult();
        //    SqlCommand command = new SqlCommand();
        //    command.CommandType = CommandType.Text;
        //    command.Connection = sqlcon;
        //    command.CommandText = SQL;
        //    SqlDataAdapter adpter = new SqlDataAdapter(command);
        //    try
        //    {
        //        adpter.Fill(dt);
        //        _result.Result = ActResult.ResultType.Success;
        //    }
        //    catch (Exception)
        //    {
        //        _result.Result = ActResult.ResultType.Fail;

        //    }

        //    return _result;
        //}
    }

    public class ActResult
    {
        public ResultType Result { get; set; }
        public enum ResultType
        {
            Success = 1,
            Fail = 2
        }
        public string Message { get; set; }
    }
}
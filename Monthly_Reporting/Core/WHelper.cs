﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Monthly_Reporting.Core
{
    public class WHelper
    {
        public object Session { get; private set; }
        
        public bool Is_Checking(string Setkey,string ServerName,string DbName,string UserName,string ID)
        {
            bool Is_Checking = false;

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Setkey.ToString());
            
            //This Check Connection 
            if (this.CheckExitConnection(Convert.ToString(ServerName), Convert.ToString(DbName), Convert.ToString(UserName)) == true)
            {
                Is_Checking = true;
            }

            //Check Session login

            if (ID == null) {
                Is_Checking = true;
            }

            return Is_Checking;
        }
        public bool CheckExitConnection(string ServerName,string DbName,string UserName)
        {
            bool is_connect = false;
                
            if(ServerName == "" || DbName == "" || UserName == "")
            {
                is_connect = true;
                
            }
            return is_connect;
        }
        
        /* 
         * enum
         * Creat enum for check reports
         */
         public enum emReportType
        {
            Store,
            Function,
            View,
            StrSQl
        }
        
        public string JoinDatatable(string InputBrCode,DateTime ReportDate)
        {
            double Balamt = 0;
            DataTable dtresult = new DataTable();
            DataTable dtJsonData = JosonData(ReportDate);
            var res = dtJsonData.Rows
                      .Cast<DataRow>()
                      .Where(x => x["BrCodeCMR"].ToString().StartsWith(InputBrCode)).ToList();
            if (res.Count >= 1)
            {
                dtresult = res.CopyToDataTable();
                foreach (DataRow row in dtresult.Rows)
                {
                    Balamt = double.Parse(row["BalAmtCMR"].ToString());
                }
            }
            return Balamt.ToString();

        }

        

        
        private DataTable JosonData(DateTime ReportDate)
        {

            WebRequest wr = WebRequest.Create(new Uri("http://app.sahakrinpheap.com/skp_reports/daily/JsonMonthlyChecking/" + string.Format("{0:yyyy-MM-dd}", ReportDate)));
            WebResponse ws = wr.GetResponse();
            StreamReader sr = new StreamReader(ws.GetResponseStream());
            string newversion = sr.ReadToEnd();
            var json = JsonConvert.DeserializeObject<List<GetJsonData>>(newversion);
            DataTable dt = new DataTable();

            dt.Columns.Add("BrCodeCMR");
            dt.Columns.Add("BalAmtCMR");
            //dgview.DataSource = json;
            foreach (var item in json)
            {
                dt.Rows.Add(item.BrCodeCMR, item.BalAmtCMR);
            }

            return dt;

        }
        
    }
    class GetJsonData
    {
        public int BrCodeCMR { get; set; }
        public int BalAmtCMR { get; set; }

    }
}
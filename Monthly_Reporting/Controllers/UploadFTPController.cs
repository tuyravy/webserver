﻿using Monthly_Reporting.Models;
using Monthly_Reporting.Properties;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Collections.Generic;
using ClosedXML.Excel;

namespace Monthly_Reporting.Controllers
{
    public class UploadFTPController : Controller
    {
        Utility Ut = new Utility();
        Core.WHelper helper = new Core.WHelper();
        DataTable dtSubMenu = new DataTable();
        DataTable dtMenu = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtDatabase = new DataTable();
        DataTable dtRun = new DataTable();
        readonly Reports rs = new Reports();
        Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();
        public ActionResult Is_Checking()
        {

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Session["user_key"]);
            dtSubMenu = UsAccRight.UserAccessRight(userkey);
            dtMenu = UsAccRight.Main_Menu(userkey);

            if (helper.CheckExitConnection(Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"])) == true)
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            //Check Session

            if (Session["ID"] == null) { return RedirectToAction("Index", "Login/Index"); }
            return View();
        }

        // GET: UploadFTP
        
        public ActionResult Index()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);
            
            this.LoadBackup();
            //Check Config
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);
            ViewBag.dtDatabase = dtDatabase;
            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            return View();
        }
        [HttpPost]
        public ActionResult Index(Reports rs)
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);
            if (rs.download == "download")
            {
                this.LoadBackup();
                rs.mode = 1;
                ViewBag.ReportDate = rs.reportdate;
                rs.datestart = DateTime.Now;
                rs.dateend = DateTime.Now;
                rs.BrCode = "";
                rs.disbdate = rs.reportdate;
                rs.CoId = "0";
                dtReport = dtDatabase;
                string ReportName = "DailyListing";
                ExportDataToExcel(dtReport, "_" + ReportName);
            }
            else
            {
                this.LoadBackup();
            }
            
            //Check Config
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);
            ViewBag.dtDatabase = dtDatabase;
            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            return View();
        }
        public void LoadBackup()
        {
            var fileInfo = new List<string>();
            string userkey = Convert.ToString(Session["user_key"]);
            DataTable dt = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            string FolderFormat = "_MBWin_Backup_20200930";
            DataTable dtlist = new DataTable();
            dtlist.Columns.Add("ID");
            dtlist.Columns.Add("BrCode");
            dtlist.Columns.Add("BrName");
            dtlist.Columns.Add("DatabaseName");
            int i = 0;
            
            foreach (DataRow dr in dt.Rows)
            {
                i++;
                fileInfo = Ut.FTPConnection("ftp://96.9.69.76/BRANCH_FTP/CBU", "CBU", "CBU@001",dr["BrLetter"].ToString(),dr["BrLetter"].ToString()+ FolderFormat);
               
                fileInfo.ForEach(delegate(string name){
                    dtlist.Rows.Add(i, dr["BrCode"].ToString(), dr["BrLetter"].ToString(), name.ToString());
                });
            }
            dtDatabase = dtlist.Copy();
        }

        public void ExportDataToExcel(DataTable dt, string fileName)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "sheet1");
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        //protected void Load_DataBackup(object sender, EventArgs e)
        //{
        //    //FTP Server URL.
        //    string ftp = "ftp://69.9.69.76/BRANCH_FTP/CBU";

        //    //FTP Folder name. Leave blank if you want to list files from root folder.
        //    string ftpFolder = "Uploads/";

        //    try
        //    {
        //        //Create FTP Request.
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp + ftpFolder);
        //        request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

        //        //Enter FTP Server credentials.
        //        request.Credentials = new NetworkCredential("CBU", "CBU@001");
        //        request.UsePassive = true;
        //        request.UseBinary = true;
        //        request.EnableSsl = false;

        //        //Fetch the Response and read it using StreamReader.
        //        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        //        List<string> entries = new List<string>();
        //        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //        {
        //            //Read the Response as String and split using New Line character.
        //            entries = reader.ReadToEnd().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
        //        }
        //        response.Close();

        //        //Create a DataTable.
        //        DataTable dtFiles = new DataTable();
        //        dtFiles.Columns.AddRange(new DataColumn[3] { new DataColumn("Name", typeof(string)),
        //                                            new DataColumn("Size", typeof(decimal)),
        //                                            new DataColumn("Date", typeof(string))});

        //        //Loop and add details of each File to the DataTable.
        //        foreach (string entry in entries)
        //        {
        //            string[] splits = entry.Split(new string[] { " ", }, StringSplitOptions.RemoveEmptyEntries);

        //            //Determine whether entry is for File or Directory.
        //            bool isFile = splits[0].Substring(0, 1) != "d";
        //            bool isDirectory = splits[0].Substring(0, 1) == "d";

        //            //If entry is for File, add details to DataTable.
        //            if (isFile)
        //            {
        //                dtFiles.Rows.Add();
        //                dtFiles.Rows[dtFiles.Rows.Count - 1]["Size"] = decimal.Parse(splits[4]) / 1024;
        //                dtFiles.Rows[dtFiles.Rows.Count - 1]["Date"] = string.Join(" ", splits[5], splits[6], splits[7]);
        //                string name = string.Empty;
        //                for (int i = 8; i < splits.Length; i++)
        //                {
        //                    name = string.Join(" ", name, splits[i]);
        //                }
        //                dtFiles.Rows[dtFiles.Rows.Count - 1]["Name"] = name.Trim();
        //            }
        //        }

        //    }
        //    catch (WebException ex)
        //    {
        //        throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
        //    }
        //}


    }
}

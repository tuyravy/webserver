﻿using ClosedXML.Excel;
using Monthly_Reporting.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Monthly_Reporting.Controllers
{
    public class FinanceController : Controller
    {
        Utility Ut = new Utility();

        Core.WHelper helper = new Core.WHelper();
        DataTable dtSubMenu = new DataTable();
        DataTable dtMenu = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtRun = new DataTable();

        readonly Reports rs = new Reports();
        Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();
        // GET: Finance
        public ActionResult Is_Checking()
        {

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Session["user_key"]);
            dtSubMenu = UsAccRight.UserAccessRight(userkey);
            dtMenu = UsAccRight.Main_Menu(userkey);

            if (helper.CheckExitConnection(Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"])) == true)
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            //Check Session

            if (Session["ID"] == null) { return RedirectToAction("Index", "Login/Index"); }
            return View();
        }

        //Method Get is use defual loading.
        // GET: HRModule
        [HttpGet]
        public ActionResult Index()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config


            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            return View();
        }
        public DataTable DailyChecking(DateTime ReportDate)
        {
            DataTable dtMargeZone = new DataTable();
            DataTable dtMaregCMR = new DataTable();

            string userkey = Convert.ToString(Session["user_key"]);
            dtMargeZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            dtMaregCMR.Columns.Add("ID");
            dtMaregCMR.Columns.Add("BrCode");
            dtMaregCMR.Columns.Add("BrName");
            dtMaregCMR.Columns.Add("BalAmt");
            
            int i = 0;
            foreach (DataRow dr in dtMargeZone.Rows)
            {
                i++;
                double balanceCMR = double.Parse(helper.JoinDatatable(dr["BrCode"].ToString(),ReportDate));
                dtMaregCMR.Rows.Add(i,dr["BrCode"].ToString(),dr["BrName"].ToString(),balanceCMR);
            }

            return dtMaregCMR;
        }
        
        /*Daily EOD Checking*/
        public ActionResult Eod(Reports rs)
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);
            ViewBag.ReportDate = DateTime.Now.ToString("yyyy-MM-dd");
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            //Check Config
            /*Search Data*/
            if (rs.search == "search")
            {
                ViewBag.ReportDate = rs.reportdate;                
                ViewBag.dt_ManageZone = this.DailyChecking(rs.reportdate);
            }            

            if(rs.download == "download")
            {
                string ReportName = "Daily EOD";
                ExportDataToExcel(this.DailyChecking(rs.reportdate), "_" + ReportName);
            }
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            

            return View();
        }
        
        /* Monthly Checking EOM*/
        public ActionResult EoM()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config


            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            return View();
        }
        /*
         * Adminfee Consolidate
         */
        public ActionResult Adminfeeconsolidate()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config


            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));

            return View();
        }
        [HttpPost]
        public ActionResult Adminfeeconsolidate(Reports rs)
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config


            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            DataTable dt = new DataTable();

            if(rs.download == "download")
            {
                dt = Ut.ConsolidateAdminFee(rs, userkey);
                string ReportName = "Daily_Consolidate AdminFee";
                ExportDataToExcel(dt, rs.BrCode+"_"+rs.reportdate+"_" + ReportName);
            }
            return View();
        }
        
        public void ExportDataToExcel(DataTable dt, string fileName)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "sheet1");
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}
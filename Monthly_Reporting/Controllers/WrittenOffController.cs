﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using Monthly_Reporting.Models;
using Monthly_Reporting.Properties;
using System.Configuration;
namespace Monthly_Reporting.Controllers
{
    public class WrittenOffController : Controller
    {
        readonly SqlConnection sqlcon = new SqlConnection();
        Utility mutility = new Utility();
        WriteOff wo = new WriteOff();
        ReportDateController CurrRunDate = new ReportDateController();
        readonly Reports rs = new Reports();

        // GET: WrittenOff
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();
            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select row_number() over(order by BrCode) as id,* from dbo.Rep_WrittenOff  where reportdate='2019-04-30'  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }
            }
        }
        /*
         * Search report by written off with date
         */
        [HttpPost]
        public ActionResult SearchReportWrittenOff(WriteOff wo)
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();
            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select row_number() over(order by BrCode) as id,* from dbo.Rep_WrittenOff  where reportdate='"+ String.Format("{0:yyyy-MM-dd}",wo.date_start)+@"'  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View("Index");
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }
            }
        }
        public ActionResult Sumbymoth()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            DataTable dtColumnTitle = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();
            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select row_number() over(order by BrCode) as id,* from dbo.Rep_WrittenOff  where reportdate='2019-04-30'  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    dtColumnTitle = wo.CulumnTitleWO();
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.ColumnTitle = dtColumnTitle;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }                
            }
        }
        /*
         * Search report by written off with date
         */
        [HttpPost]
        public ActionResult SearchReportWrittenOffAllMonth(WriteOff wo)
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            DataTable dtColumnTitle = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();
            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select row_number() over(order by BrCode) as id,* from dbo.Rep_WrittenOff  where  reportdate='" + String.Format("{0:yyyy-MM-dd}", wo.date_start) + @"'  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.date_start = wo.date_start;
                    ViewBag.ColumnTitle = dtColumnTitle;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View("Sumbymoth");
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }
            }
        }
        [HttpPost]
        public ActionResult SearchReportWrittenOffCompare(WriteOff wo)
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            DataTable dtColumnTitle = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();
            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select row_number() over(order by BrCode) as id,* from Rep_WrittenOffMBWIN  where  reportdate='" + String.Format("{0:yyyy-MM-dd}", wo.date_start) + @"'  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.date_start = wo.date_start;
                    ViewBag.ColumnTitle = dtColumnTitle;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View("GlComTool");
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }
            }
        }
        public ActionResult GlComTool()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();

            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    string sql = "select  row_number() over(order by BrCode) as id, * from dbo.Rep_WrittenOffMBWIN  order by BrCode ASC";
                    dtwrittenoff = wo.Write_Off(sql);
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.writtenoff = dtwrittenoff;
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }
        public DataTable Brlist(string userkey)
        {
            DataTable dt = new DataTable();
            string branchZone = Convert.ToString(mutility.dbSingleResult("select branch_zone from users where user_key='"+ userkey + "'"));
            var zone_array = branchZone.Split(',');
            string zonetoarray = string.Join("','", zone_array.Skip(0));
            string brlist = "select * from BRANCH_LISTS where flag=1 and BrCode in('" + zonetoarray + "');";
            dt= mutility.dbResult(brlist);
            return dt;
           

        }
        public DataTable DtDaily(string brcode,DateTime datestart,DateTime dateend)
        {
            DataTable dt = new DataTable();
            string SQL = "";
            if (brcode == "ALL")
            {
                SQL = @"
                        declare @ReportEnd datetime
                        declare @ReportStart datetime
						declare @Reportdate datetime

						set @Reportdate='2019-06-30';
                        set @ReportStart='" + string.Format("{0:yyyy-MM-dd}", datestart) + @"';
                        set @ReportEnd='" + string.Format("{0:yyyy-MM-dd}", dateend) + @"';
                        if object_id('tempdb..#WO') is not null
	                        drop table #WO

                        select 
	                        bl.Brletter,
	                        bl.BrCode,	                       
	                        sum(case 
			                        when isnull(([dbo].[fn_GetTotalCurrBalAmtWO](c.BrCode,c.Acc,@ReportDate)),0)<0 then 0
		                        else 
			                        isnull(([dbo].[fn_GetTotalCurrBalAmtWO](c.BrCode,c.Acc,@ReportDate)),0)
		                        end) as AmtToClose,
	                        sum(c.TotalCollectedAmt) as TotalCollectedAmt,
							(select isnull(sum(gl.TrnAmt),0) from [dbo].[FN_GLTransaction] gl where gl.reportdate between @ReportStart and @ReportEnd and gl.GLAcc='5744011' and gl.brcode=bl.brcode) as GLAmt
	                        into #WO
	                        from [dbo].[WO_COLLECTION] c
	                        inner join [dbo].[BRANCH_LISTS] bl on c.BrCode=bl.BrCode
	                         where reportdate=@Reportdate and 
	                        collectdate between @ReportStart and @ReportEnd 
	                        group by bl.Brletter,bl.BrCode
							
							select * from #WO";
            }
            else
            {
                SQL = @"
                        declare @ReportEnd datetime
                        declare @ReportStart datetime
						declare @Reportdate datetime

						set @Reportdate='2019-06-30';
                        set @ReportStart='" + string.Format("{0:yyyy-MM-dd}", datestart) + @"';
                        set @ReportEnd='" + string.Format("{0:yyyy-MM-dd}", dateend) + @"';
                        if object_id('tempdb..#WO') is not null
	                        drop table #WO

                        select 
	                        bl.Brletter,
	                        bl.BrCode,	                       
	                        sum(case 
			                        when isnull(([dbo].[fn_GetTotalCurrBalAmtWO](c.BrCode,c.Acc,@ReportDate)),0)<0 then 0
		                        else 
			                        isnull(([dbo].[fn_GetTotalCurrBalAmtWO](c.BrCode,c.Acc,@ReportDate)),0)
		                        end) as AmtToClose,
	                        sum(c.TotalCollectedAmt) as TotalCollectedAmt,
							(select isnull(sum(gl.TrnAmt),0) from [dbo].[FN_GLTransaction] gl where gl.reportdate between @ReportStart and @ReportEnd and gl.GLAcc='5744011' and gl.brcode=bl.brcode) as GLAmt
	                        into #WO
	                        from [dbo].[WO_COLLECTION] c
	                        inner join [dbo].[BRANCH_LISTS] bl on c.BrCode=bl.BrCode
	                         where reportdate=@Reportdate and 
	                        collectdate between @ReportStart and @ReportEnd and bl.BrCode='"+brcode+@"'
	                        group by bl.Brletter,bl.BrCode							
							select * from #WO";
            }
            
            dt=mutility.dbResult(SQL);
            return dt;
        }

        //Daily collection
        public ActionResult DailyCollection()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            DataTable dtDaily = new DataTable();
            DateTime datestart = DateTime.Now;
            DateTime dateend = DateTime.Now;
            
            ViewBag.datestart = datestart;
            ViewBag.dateend = dateend;

            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    ViewBag.Data = dtDaily;
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }
        //Daily Exec 
        public ActionResult ExecDailyCollection(Reports re)
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
            DataTable dtDaily = new DataTable();
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();

                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;

                    DateTime datestart =re.datestart;
                    DateTime dateend = re.dateend;
                    ViewBag.datestart = datestart;
                    ViewBag.dateend = dateend;

                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    ViewBag.Data = this.DtDaily(re.BrCode,re.datestart,re.dateend);
                    return View("DailyCollection");
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }

        //Monthly collection
        public ActionResult MonthlyCollection()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
           
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                   
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }

        //Exec Monthly collection
        public ActionResult ExecMonthlyCollection()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();

            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();

                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }

        //Monthly collection
        public ActionResult SummaryReports()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();
          
            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();
                    
                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
        }

        //Add GIS location by customer
        public ActionResult AddGis_Writteoff()
        {
            DataTable dt = new DataTable();
            DataTable dt_main_menu = new DataTable();
            DataTable dtwrittenoff = new DataTable();

            mutility.DbName = Settings.Default["DbName"].ToString();
            mutility.UserName = Settings.Default["UserName"].ToString();
            mutility.Password = Settings.Default["Password"].ToString();
            mutility.ServerName = Settings.Default["ServerName"].ToString();

            if (mutility.ServerName == "" || mutility.DbName == "" || mutility.UserName == "")
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }
            else
            {
                if (Session["ID"] != null)
                {
                    UserAccessRightController UsAccRight = new UserAccessRightController();

                    string userkey = Session["user_key"].ToString();
                    dt = UsAccRight.UserAccessRight(userkey);
                    dt_main_menu = UsAccRight.Main_Menu(userkey);
                    ViewBag.manin_menu = dt_main_menu;
                    ViewBag.sub_manin = dt;
                    ViewBag.dt_ManageZone = this.Brlist(userkey);
                    ViewBag.CurrRunDate = CurrRunDate.CurrRunDate(userkey);
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login/Index");
                }


            }
            
        }

        
    }
}
    
    
   
    
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClosedXML.Excel;
using Monthly_Reporting.Models;
using Monthly_Reporting.Properties;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Mvc;
namespace Monthly_Reporting.Controllers
{
    public class BuildReportsController : Controller
    {
        readonly SqlConnection sqlcon = new SqlConnection();
        Utility Ut = new Utility();
        readonly Reports rs = new Reports();
        ReportDateController CurrRunDate = new ReportDateController();

        Core.WHelper helper = new Core.WHelper();
        DataTable dt = new DataTable();
        DataTable dt_main_menu = new DataTable();
        DataTable dt_Report = new DataTable();
        DataTable dtRun = new DataTable();

        string BrCode = "";
        //Public Constructor
        public ActionResult Is_Checking()
        {

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Session["user_key"]);
            dt = UsAccRight.UserAccessRight(userkey);
            dt_main_menu = UsAccRight.Main_Menu(userkey);

            if (helper.CheckExitConnection(Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"])) == true)
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            //Check Session

            if (Session["ID"] == null) { return RedirectToAction("Index", "Login/Index"); }
            return View();
        }
        // GET: BuildReports
        // Main Report (Include all Report for Operation and Finnace
        public ActionResult Index()
        {
            //Check Config
            this.Is_Checking();

            string userkey = Convert.ToString(Session["user_key"]);
            ViewBag.manin_menu = dt_main_menu;
            ViewBag.sub_manin = dt;

            ViewBag.dt_Report = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["ReportCode"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            ViewBag.dtRun = dtRun;
            return View();            
        }
    }
}
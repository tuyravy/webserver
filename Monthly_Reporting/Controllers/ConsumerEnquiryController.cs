﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Monthly_Reporting.Models;
namespace Monthly_Reporting.Controllers
{
    public class ConsumerEnquiryController : Controller
    {
        Utility Ut = new Utility();
        Core.WHelper helper = new Core.WHelper();
        DataTable dtSubMenu = new DataTable();
        DataTable dtMenu = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtRun = new DataTable();
        readonly Reports rs = new Reports();

        Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();
        public ActionResult Is_Checking()
        {

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Session["user_key"]);
            dtSubMenu = UsAccRight.UserAccessRight(userkey);
            dtMenu = UsAccRight.Main_Menu(userkey);

            if (helper.CheckExitConnection(Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"])) == true)
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            //Check Session

            if (Session["ID"] == null) { return RedirectToAction("Index", "Login/Index"); }
            return View();
        }
        // GET: ConsumerEnquiry
        public ActionResult Index()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config

            ViewBag.message = "No";
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtLocationCode = Ut.LocationCode(1,1,"0");
            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            return View();
        }
        public ActionResult Enquiry(Reports rs)
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config


            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);
            ViewBag.message = "No";
            ViewBag.dtLocationCode = Ut.LocationCode(1, 1, "0");
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            if (rs.submit == "submit")
            {
                //ViewBag.message = "ព័ត៌មានដែលអ្នកស្វែងរកមិនមានទេ";
                return RedirectToAction("ViewEnquiry");
            }
            return View("Index");
        }
        
        public string DisplayDistrict(string ProvinceCode)
        {
            DataTable dtDistrict = new DataTable();
            dtDistrict = Ut.LocationCode(2, 1, ProvinceCode);
            string District_display = "";
            District_display = "<option value = '0' selected='selected'> ជ្រើសរើសស្រុក </ option >";
            foreach (DataRow dr in dtDistrict.Rows)
            {
                
                District_display += "<option value=" + dr["DCode"].ToString() + ">" + dr["District"].ToString() + "</option>";
            }
            return District_display;
        }
        public string DisplayCommune(string DistrictCode)
        {
            DataTable dtCommune = new DataTable();
            dtCommune = Ut.LocationCode(3, 1, DistrictCode);
            string Commune_display = "";
            Commune_display = "<option value = '0' selected='selected'> ជ្រើសរើសឃុំសង្កាត់ </ option >";
            foreach (DataRow dr in dtCommune.Rows)
            {
                
                Commune_display += "<option value=" + dr["CCode"].ToString() + ">" + dr["Commune"].ToString() + "</option>";
            }
            return Commune_display;
        }
        
        public ActionResult ViewEnquiry(Reports rs)
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config

            ViewBag.message = "No";
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtLocationCode = Ut.LocationCode(1, 1, "0");
            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            return View();
        }
    }
}
﻿using ClosedXML.Excel;
using Monthly_Reporting.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Monthly_Reporting.Controllers
{
    public class HRController : Controller
    {
        Utility Ut = new Utility();
        Core.WHelper helper = new Core.WHelper();
        DataTable dtSubMenu = new DataTable();
        DataTable dtMenu = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtRun = new DataTable();
        readonly Reports rs = new Reports();
        Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();
        public ActionResult Is_Checking()
        {

            Controllers.UserAccessRightController UsAccRight = new Controllers.UserAccessRightController();

            string userkey = Convert.ToString(Session["user_key"]);
            dtSubMenu = UsAccRight.UserAccessRight(userkey);
            dtMenu = UsAccRight.Main_Menu(userkey);

            if (helper.CheckExitConnection(Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"])) == true)
            {
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            //Check Session

            if (Session["ID"] == null) { return RedirectToAction("Index", "Login/Index"); }
            return View();
        }
        //Method Get is use defual loading.
        // GET: HRModule
        public ActionResult Index()
        {
            //Check Config
            this.Is_Checking();
            string userkey = Convert.ToString(Session["user_key"]);

            //Check Config
            
            
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
           
            return View();
        }
        /*
            #Create by Tuy Ravy
            #This function create for user month post when search data ,Download.
            
        */
        DataSet dsPublic = new DataSet();
        [HttpPost]
        public ActionResult index(Reports rs)
        {
            DataTable dtReport = new DataTable();
            
            string userkey = Convert.ToString(Session["user_key"]);

            this.Is_Checking();

            /*Search Data*/
            if (rs.search == "search")
            {
                //Sumbit Data
                ViewBag.CompanyType = rs.CompanyType.Replace(" ", "");
                ViewBag.ReportCode = rs.ReportCode.Replace(" ", "");
                ViewBag.ReportDate = rs.reportdate;
                rs.mode = 1;
                
                rs.datestart = DateTime.Now;
                rs.dateend = DateTime.Now;
                rs.BrCode = "";
                rs.disbdate = rs.reportdate;
                rs.CoId = "0";
                dtReport = Ut.CoreReport(rs);                
                
            }
            
            if (rs.download == "download")
            {
                ViewBag.CompanyType = rs.CompanyType.Replace(" ", "");
                ViewBag.ReportCode = rs.ReportCode.Replace(" ", "");
                rs.mode = 1;
                ViewBag.ReportDate = rs.reportdate;
                rs.datestart = DateTime.Now;
                rs.dateend = DateTime.Now;
                rs.BrCode = "";
                rs.disbdate = rs.reportdate;
                rs.CoId = "0";
                dtReport = Ut.CoreReport(rs);
                string ReportName = "DailyListing";                
                ExportDataToExcel(dtReport,"_" + ReportName);
            }
            

            /*Menu Display*/
            ViewBag.manin_menu = UsAccRight.Main_Menu(userkey);
            ViewBag.sub_manin = UsAccRight.UserAccessRight(userkey);

            /*Display Report by User and by Zone*/
            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["report_code"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
           

            return View();
        }
        public void ExportDataToExcel(DataTable dt, string fileName)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "sheet1");
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}
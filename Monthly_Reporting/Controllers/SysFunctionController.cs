﻿using ClosedXML.Excel;
using Monthly_Reporting.Models;
using Monthly_Reporting.Properties;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Monthly_Reporting.Controllers
{
 
    public class SysFunctionController : Controller
    {
        Utility Ut = new Utility();
        Core.WHelper helper = new Core.WHelper();
        DataTable dtSubMenu = new DataTable();
        DataTable dtMenu = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtRun = new DataTable();

        // GET: SysFunction
        public ActionResult Index()
        {
            string userkey = Convert.ToString(Session["user_key"]);
            //Check Config
            if(helper.Is_Checking(userkey, Convert.ToString(Session["ServerName"]), Convert.ToString(Session["DbName"]), Convert.ToString(Session["UserName"]), Convert.ToString(Session["ID"])) == true){
                return RedirectToAction("Index", "Setting_Connection/Index");
            }

            ViewBag.manin_menu = dtMenu;
            ViewBag.sub_manin = dtSubMenu;

            ViewBag.dtReport = Ut.ListReport(Ut.ArrayOfReportList(Convert.ToString(Session["ReportCode"])));
            ViewBag.dt_ManageZone = Ut.ZoneControl(Ut.ArrayOfZoneList(userkey));
            ViewBag.dtRun = dtRun;
            return View();
        }
    }
}